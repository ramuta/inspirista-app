package me.ramuta.inspirista.classes;

public class InstagramData {
	public static final String CLIENT_ID = "your-id"; // get your id and secret key at instagram developers web page
	public static final String CLIENT_SECRET = "secret-key";
	public static final String CALLBACK_URL = "instagram://connect";
}
